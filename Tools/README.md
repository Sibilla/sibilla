## The Tools folder contains



- **ABT** (to unzip)
    - Executable folder
    - Configuration file _settings.config_
        - **number_of_episodes**: the number of episodes (test cases) to reproduce (set to _30_ for the experiments)
        - **number_of_steps**: the number of steps (actions/events) to reproduce (set to _50_ for the experiments)
        - **dataSource**: the SQL Server where the subject application test database is placed (used as input data source for test cases when testing PHP-based Web apps, i.e., MRBS, Dolibarr, Mantis Bug Tracker. You can leave it empty in case of problems with database configuration and ABT will generate random input data)
        - **username** and **password**: the credentials to access the SQL Server (used as input data source for test cases when testing PHP-based Web apps, i.e., MRBS, Dolibarr, Mantis Bug Tracker. You can leave it empty in case of problems with database configuration and ABT will generate random input data)
        - **initialCatalog**: the name of the subject application test database (refer to the exact name of the database placed under SQL Server Database folder of the subject application, see [Subjects](https://gitlab.com/Sibilla/sibilla/-/tree/master/Subjects), e.g., _bugtracker_disjoints_ for Mantis)
        - **application**: the url of the initial page of the subject application (e.g., _http://localhost/mantisbt/login_page.php_).
        - **lemmatizer**: the file system path to the lemmatizer folder used for syntactic comparison (needed to fill forms with meaningful data, see Dependencies below)
        - **word2vecModel**: the file system path to the word2vec model file used for semantics comparisons (needed to fill forms with meaningful data, see Dependencies below)
        - **browserDriverPath**: the file sytem path to the Chrome driver folder used to interact with the browser (see Dependencies below)
        - **scanningWidgetsFilter**: the file system path to the filtering file specific of the app used to ignore/consider web elements within pages (see Dependencies below)
        - **outputPath**: the file system path to the ABT output folder (it must be created before the first execution)
        - **explorationTechnique**: the tecnique used by ABT to explore the subject application (_random_ or _qlearning_)
        - **useCaching**: boolean value. It must be set to _true_ if Sibilla has to be applied, _false_ in case of Baseline
        - **fixedActions**: boolean value. It must be set to _true_ if Sibilla has to replicate the actions saved from a previous run, _false_ otherwise
        - **numRun**: the number of the iteration run (i.e, between _1_ and _10_ for the experiments)
        - **stateAbstractionMode**: the kind of Sibilla abstraction to apply over GUI states (i.e., _titleUrl_, _enabledness_, _identity_)
        - **actionAbstractionMode**: the kind of Sibilla abstraction to apply over GUI actions (i.e., _numElements_, _identification_, _identity_)
        - **numSlots**: the capacity of Sibilla target states list provided while inferring states (i.e., _1_ or _5_)
        - **okkoPath**: the path to ok-ko.xml file used to compute forms submissions at runtime 
        - **appName**: the name of the application under test (i.e., _tricentis_, _mantis_, _dolibarr_, _mrbs_, _dimeshift_, _pagekit_, _petclinic_, _retrospected_, _splittypie_)
    - Dependencies folder
        - **BrowserDriver**: it must contain the ChromeDriver needed to run ABT on the version of Chrome browser runned by the machine. It can be downloaded from https://chromedriver.chromium.org/downloads (to unzip)
        - **Filters**: it contains the subject applications filtering files needed to ignore/consider GUI widgets within pages
        - **Lemmatizer**: it contains the file needed to perform syntactic words comparisons
        - **Word2Vec**: it must cointain the model needed to perform semantic words comparisons. It can be downloaded from https://github.com/eyaler/word2vec-slim/blob/master/GoogleNews-vectors-negative300-SLIM.bin.gz (to unzip)
- **Crawljax** (to unzip)
    - Executable jar file
    - Configuration file _settings.xml_
        - **numMaxEpisodes**: the number of episodes (test cases) to reproduce (set to _30_ for the experiments)
        - **numMaxSteps**: the number of steps (actions/events) to reproduce (set to _50_ for the experiments)
        - **localizationMode**: the modes the elements are extracted from web pages (i.e., _scanning_ for Baseline, _caching_ for Sibilla) 
        - **url**: the url of the initial page of the subject application (e.g., _http://localhost/mantisbt/login_page.php_) 
        - **applicationName**: the name of the application under test (i.e., _tricentis_, _mantis_, _dolibarr_, _mrbs_, _dimeshift_, _pagekit_, _petclinic_, _retrospected_, _splittypie_)
        - **outputPath**: the file system path to the Crawljax output folder
        - **fixedActionsMode**: boolean value. It must be set to _true_ if Sibilla has to replicate the actions saved from a previous run, _false_ otherwise
        - **runNumber**: the number of the iteration run (i.e, between _1_ and _10_ for the experiments)
        - **stateAbstractionMode**: the kind of Sibilla abstraction to apply over GUI states (i.e., _titleUrl_, _enabledness_, _identity_)
        - **actionAbstractionMode**: the kind of Sibilla abstraction to apply over GUI actions (i.e., _numElements_, _identification_, _identity_)
        - **numCachedLists**: the capacity of Sibilla target states list provided while inferring states (i.e., _1_ or _5_)
        - **browserDriverPath**: the path to external browser driver
        - **formsSubmissionsChecks**: the path to ok-ko.xml file used to compute forms submissions at runtime 
        - **browser**: the type of browser to use (e.g., chrome, firefox)
       

**Notes**
To run the tools with the same configuration of the expertiments, keep all the unmentioned parameters unaltered. The versions here provided of ABT and Crawljax integrated with Sibilla have been developed and tested on Windows machines.
