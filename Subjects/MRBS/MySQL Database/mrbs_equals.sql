-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ago 10, 2021 alle 15:29
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mrbs`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_area`
--

CREATE TABLE `mrbs_area` (
  `id` int(11) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT 0,
  `area_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_key` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `timezone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_admin_email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` int(11) DEFAULT NULL,
  `default_duration` int(11) DEFAULT NULL,
  `default_duration_all_day` tinyint(4) NOT NULL DEFAULT 0,
  `morningstarts` int(11) DEFAULT NULL,
  `morningstarts_minutes` int(11) DEFAULT NULL,
  `eveningends` int(11) DEFAULT NULL,
  `eveningends_minutes` int(11) DEFAULT NULL,
  `private_enabled` tinyint(4) DEFAULT NULL,
  `private_default` tinyint(4) DEFAULT NULL,
  `private_mandatory` tinyint(4) DEFAULT NULL,
  `private_override` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_create_ahead_enabled` tinyint(4) DEFAULT NULL,
  `min_create_ahead_secs` int(11) DEFAULT NULL,
  `max_create_ahead_enabled` tinyint(4) DEFAULT NULL,
  `max_create_ahead_secs` int(11) DEFAULT NULL,
  `min_delete_ahead_enabled` tinyint(4) DEFAULT NULL,
  `min_delete_ahead_secs` int(11) DEFAULT NULL,
  `max_delete_ahead_enabled` tinyint(4) DEFAULT NULL,
  `max_delete_ahead_secs` int(11) DEFAULT NULL,
  `max_per_day_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_per_day` int(11) NOT NULL DEFAULT 0,
  `max_per_week_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_per_week` int(11) NOT NULL DEFAULT 0,
  `max_per_month_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_per_month` int(11) NOT NULL DEFAULT 0,
  `max_per_year_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_per_year` int(11) NOT NULL DEFAULT 0,
  `max_per_future_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_per_future` int(11) NOT NULL DEFAULT 0,
  `max_secs_per_day_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_secs_per_day` int(11) NOT NULL DEFAULT 0,
  `max_secs_per_week_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_secs_per_week` int(11) NOT NULL DEFAULT 0,
  `max_secs_per_month_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_secs_per_month` int(11) NOT NULL DEFAULT 0,
  `max_secs_per_year_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_secs_per_year` int(11) NOT NULL DEFAULT 0,
  `max_secs_per_future_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_secs_per_future` int(11) NOT NULL DEFAULT 0,
  `max_duration_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `max_duration_secs` int(11) NOT NULL DEFAULT 0,
  `max_duration_periods` int(11) NOT NULL DEFAULT 0,
  `custom_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval_enabled` tinyint(4) DEFAULT NULL,
  `reminders_enabled` tinyint(4) DEFAULT NULL,
  `enable_periods` tinyint(4) DEFAULT NULL,
  `periods` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_enabled` tinyint(4) DEFAULT NULL,
  `confirmed_default` tinyint(4) DEFAULT NULL,
  `times_along_top` tinyint(4) NOT NULL DEFAULT 0,
  `default_type` char(1) NOT NULL DEFAULT 'E'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_area`
--

INSERT INTO `mrbs_area` (`id`, `disabled`, `area_name`, `sort_key`, `timezone`, `area_admin_email`, `resolution`, `default_duration`, `default_duration_all_day`, `morningstarts`, `morningstarts_minutes`, `eveningends`, `eveningends_minutes`, `private_enabled`, `private_default`, `private_mandatory`, `private_override`, `min_create_ahead_enabled`, `min_create_ahead_secs`, `max_create_ahead_enabled`, `max_create_ahead_secs`, `min_delete_ahead_enabled`, `min_delete_ahead_secs`, `max_delete_ahead_enabled`, `max_delete_ahead_secs`, `max_per_day_enabled`, `max_per_day`, `max_per_week_enabled`, `max_per_week`, `max_per_month_enabled`, `max_per_month`, `max_per_year_enabled`, `max_per_year`, `max_per_future_enabled`, `max_per_future`, `max_secs_per_day_enabled`, `max_secs_per_day`, `max_secs_per_week_enabled`, `max_secs_per_week`, `max_secs_per_month_enabled`, `max_secs_per_month`, `max_secs_per_year_enabled`, `max_secs_per_year`, `max_secs_per_future_enabled`, `max_secs_per_future`, `max_duration_enabled`, `max_duration_secs`, `max_duration_periods`, `custom_html`, `approval_enabled`, `reminders_enabled`, `enable_periods`, `periods`, `confirmation_enabled`, `confirmed_default`, `times_along_top`, `default_type`) VALUES
(1, 0, 'Area1', 'Area1', 'Europe/London', NULL, 1800, 3600, 0, 7, 0, 18, 30, 0, 0, 0, 'none', 0, 0, 0, 604800, 0, 0, 0, 604800, 0, 1, 0, 5, 0, 10, 0, 50, 0, 100, 0, 7200, 0, 36000, 0, 90000, 0, 360000, 0, 360000, 0, 7200, 2, NULL, 0, 1, 0, '[\"Period 1\",\"Period 2\"]', 1, 1, 0, 'I'),
(2, 0, 'Area2', 'Area2', 'Europe/London', NULL, 1800, 3600, 0, 7, 0, 18, 30, 0, 0, 0, 'none', 0, 0, 0, 604800, 0, 0, 0, 604800, 0, 1, 0, 5, 0, 10, 0, 50, 0, 100, 0, 7200, 0, 36000, 0, 90000, 0, 360000, 0, 360000, 0, 7200, 2, NULL, 0, 1, 0, '[\"Period 1\",\"Period 2\"]', 1, 1, 0, 'I'),
(3, 0, 'Area3', 'Area3', 'Europe/London', NULL, 1800, 3600, 0, 7, 0, 18, 30, 0, 0, 0, 'none', 0, 0, 0, 604800, 0, 0, 0, 604800, 0, 1, 0, 5, 0, 10, 0, 50, 0, 100, 0, 7200, 0, 36000, 0, 90000, 0, 360000, 0, 360000, 0, 7200, 2, NULL, 0, 1, 0, '[\"Period 1\",\"Period 2\"]', 1, 1, 0, 'I'),
(4, 1, 'Area4', 'Area4', 'Europe/London', '', 1800, 3600, 0, 7, 0, 18, 30, 0, 0, 0, 'none', 0, 0, 0, 604800, 0, 0, 0, 604800, 0, 1, 0, 5, 0, 10, 0, 50, 0, 100, 0, 7200, 0, 36000, 0, 90000, 0, 360000, 0, 360000, 0, 7200, 2, NULL, 0, 1, 0, '[\"Period 1\",\"Period 2\"]', 1, 1, 0, 'I');

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_entry`
--

CREATE TABLE `mrbs_entry` (
  `id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT 0 COMMENT 'Unix timestamp',
  `end_time` int(11) NOT NULL DEFAULT 0 COMMENT 'Unix timestamp',
  `entry_type` int(11) NOT NULL DEFAULT 0,
  `repeat_id` int(11) DEFAULT NULL,
  `room_id` int(11) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_by` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_by` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'E',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `reminded` int(11) DEFAULT NULL,
  `info_time` int(11) DEFAULT NULL,
  `info_user` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ical_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ical_sequence` smallint(6) NOT NULL DEFAULT 0,
  `ical_recur_id` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_registration` tinyint(4) NOT NULL DEFAULT 0,
  `registrant_limit` int(11) NOT NULL DEFAULT 0,
  `registrant_limit_enabled` tinyint(4) NOT NULL DEFAULT 1,
  `registration_opens` int(11) NOT NULL DEFAULT 1209600 COMMENT 'Seconds before the start time',
  `registration_opens_enabled` tinyint(4) NOT NULL DEFAULT 0,
  `registration_closes` int(11) NOT NULL DEFAULT 0 COMMENT 'Seconds before the start_time',
  `registration_closes_enabled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_entry`
--

INSERT INTO `mrbs_entry` (`id`, `start_time`, `end_time`, `entry_type`, `repeat_id`, `room_id`, `timestamp`, `create_by`, `modified_by`, `name`, `type`, `description`, `status`, `reminded`, `info_time`, `info_user`, `info_text`, `ical_uid`, `ical_sequence`, `ical_recur_id`, `allow_registration`, `registrant_limit`, `registrant_limit_enabled`, `registration_opens`, `registration_opens_enabled`, `registration_closes`, `registration_closes_enabled`) VALUES
(1, 1628575200, 1628582400, 1, 1, 1, '2021-08-10 13:20:29', 'admin', '', 'Event1', 'I', 'Event1 Description', 0, NULL, NULL, NULL, NULL, 'MRBS-61127d1da7b4d-ebad16b3@localhost', 0, '20210810T060000Z', 1, 6, 1, 1209600, 0, 0, 0),
(2, 1631253600, 1631260800, 1, 1, 1, '2021-08-10 13:20:29', 'admin', '', 'Event1', 'I', 'Event1 Description', 0, NULL, NULL, NULL, NULL, 'MRBS-61127d1da7b4d-ebad16b3@localhost', 0, '20210910T060000Z', 1, 6, 1, 1209600, 0, 0, 0),
(3, 1628587800, 1628600400, 0, NULL, 1, '2021-08-10 13:21:02', 'admin', '', 'Event2', 'E', '', 4, NULL, NULL, NULL, NULL, 'MRBS-61127d3ec7e03-9a774c0e@localhost', 0, NULL, 1, 4, 1, 1209600, 0, 0, 0),
(4, 1628602200, 1628604000, 0, NULL, 2, '2021-08-10 13:21:19', 'admin', '', 'Event3', 'I', '', 4, NULL, NULL, NULL, NULL, 'MRBS-61127d4f32d2f-9a719d62@localhost', 0, NULL, 0, 0, 1, 1209600, 0, 0, 0),
(5, 1628582400, 1628604000, 0, NULL, 4, '2021-08-10 13:21:49', 'admin', '', 'Event4', 'I', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127d6ddf3b3-05962a06@localhost', 0, NULL, 1, 6, 1, 1209600, 0, 0, 0),
(6, 1628607600, 1628614800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210810T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(7, 1628694000, 1628701200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210811T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(8, 1628780400, 1628787600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210812T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(9, 1628866800, 1628874000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210813T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(10, 1628953200, 1628960400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210814T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(11, 1629039600, 1629046800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210815T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(12, 1629126000, 1629133200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210816T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(13, 1629212400, 1629219600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210817T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(14, 1629298800, 1629306000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210818T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(15, 1629385200, 1629392400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210819T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(16, 1629471600, 1629478800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210820T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(17, 1629558000, 1629565200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210821T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(18, 1629644400, 1629651600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210822T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(19, 1629730800, 1629738000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210823T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(20, 1629817200, 1629824400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210824T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(21, 1629903600, 1629910800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210825T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(22, 1629990000, 1629997200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210826T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(23, 1630076400, 1630083600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210827T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(24, 1630162800, 1630170000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210828T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(25, 1630249200, 1630256400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210829T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(26, 1630335600, 1630342800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210830T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(27, 1630422000, 1630429200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210831T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(28, 1630508400, 1630515600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210901T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(29, 1630594800, 1630602000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210902T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(30, 1630681200, 1630688400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210903T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(31, 1630767600, 1630774800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210904T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(32, 1630854000, 1630861200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210905T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(33, 1630940400, 1630947600, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210906T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(34, 1631026800, 1631034000, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210907T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(35, 1631113200, 1631120400, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210908T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(36, 1631199600, 1631206800, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210909T150000Z', 0, 0, 1, 1209600, 0, 0, 0),
(37, 1631286000, 1631293200, 1, 2, 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0, '20210910T150000Z', 0, 0, 1, 1209600, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_login`
--

CREATE TABLE `mrbs_login` (
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_login`
--

INSERT INTO `mrbs_login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_participants`
--

CREATE TABLE `mrbs_participants` (
  `id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registered` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_participants`
--

INSERT INTO `mrbs_participants` (`id`, `entry_id`, `username`, `create_by`, `registered`) VALUES
(1, 1, 'testuser1', 'admin', 1628601874),
(2, 1, 'testuser2', 'admin', 1628601881),
(3, 3, 'admin', 'admin', 1628601893),
(4, 5, 'admin', 'admin', 1628601932);

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_repeat`
--

CREATE TABLE `mrbs_repeat` (
  `id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT 0 COMMENT 'Unix timestamp',
  `end_time` int(11) NOT NULL DEFAULT 0 COMMENT 'Unix timestamp',
  `rep_type` int(11) NOT NULL DEFAULT 0,
  `end_date` int(11) NOT NULL DEFAULT 0 COMMENT 'Unix timestamp',
  `rep_opt` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `room_id` int(11) NOT NULL DEFAULT 1,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_by` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_by` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'E',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rep_interval` smallint(6) NOT NULL DEFAULT 1,
  `month_absolute` smallint(6) DEFAULT NULL,
  `month_relative` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `reminded` int(11) DEFAULT NULL,
  `info_time` int(11) DEFAULT NULL,
  `info_user` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ical_uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ical_sequence` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_repeat`
--

INSERT INTO `mrbs_repeat` (`id`, `start_time`, `end_time`, `rep_type`, `end_date`, `rep_opt`, `room_id`, `timestamp`, `create_by`, `modified_by`, `name`, `type`, `description`, `rep_interval`, `month_absolute`, `month_relative`, `status`, `reminded`, `info_time`, `info_user`, `info_text`, `ical_uid`, `ical_sequence`) VALUES
(1, 1628575200, 1628582400, 3, 1631253600, '0', 1, '2021-08-10 13:20:29', 'admin', '', 'Event1', 'I', 'Event1 Description', 1, 10, NULL, 0, NULL, NULL, NULL, NULL, 'MRBS-61127d1da7b4d-ebad16b3@localhost', 0),
(2, 1628607600, 1628614800, 1, 1631286000, '0', 1, '2021-08-10 13:23:11', 'admin', '', 'Event5', 'E', '', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'MRBS-61127dbf86f6b-ca3a4396@localhost', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_room`
--

CREATE TABLE `mrbs_room` (
  `id` int(11) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT 0,
  `area_id` int(11) NOT NULL DEFAULT 0,
  `room_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort_key` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacity` int(11) NOT NULL DEFAULT 0,
  `room_admin_email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invalid_types` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON encoded',
  `custom_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_room`
--

INSERT INTO `mrbs_room` (`id`, `disabled`, `area_id`, `room_name`, `sort_key`, `description`, `capacity`, `room_admin_email`, `invalid_types`, `custom_html`) VALUES
(1, 0, 1, 'Room1', 'Room1', 'Room1 Area1 Description', 7, 'area1@gmail.com', NULL, NULL),
(2, 0, 1, 'Room2', 'Room2', 'Room2 Area1 Description', 4, 'area1@gmail.com', NULL, NULL),
(3, 0, 2, 'Room Big A2', 'Room Big A2', 'Room Big A2 Description', 55, 'area2@gmail.com', NULL, NULL),
(4, 0, 1, 'Room3', 'Room3', 'Room3 Description (disabled)', 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_sessions`
--

CREATE TABLE `mrbs_sessions` (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED DEFAULT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_sessions`
--

INSERT INTO `mrbs_sessions` (`id`, `access`, `data`) VALUES
('skuambc7jmkpqsohto6mt5007r', 1628601938, 'csrf_token|s:64:\"6314c2e186f8abc73a41daa6d048ca29d60d6786b65f315f46b01159057c2d61\";');

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_users`
--

CREATE TABLE `mrbs_users` (
  `id` int(11) NOT NULL,
  `level` smallint(6) NOT NULL DEFAULT 0,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `last_login` int(11) NOT NULL DEFAULT 0,
  `reset_key_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_key_expiry` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_users`
--

INSERT INTO `mrbs_users` (`id`, `level`, `name`, `display_name`, `password_hash`, `email`, `timestamp`, `last_login`, `reset_key_hash`, `reset_key_expiry`) VALUES
(1, 2, 'admin', 'admin', '$2y$10$/82JXMbO9Usus4HYot5ASeAlrM62rgNIVRQJCF9WASt/oU2xhK0Le', 'admin@admin.com', '2021-07-29 16:25:18', 1628601867, NULL, 0),
(2, 1, 'testuser1', 'Test User1 Name', '$2y$10$Jto1Fr9ULmaI22IDHoiRzewDFQyEOF.14FX6i8EC55sV6888qMJ.i', 'testuser1@gmail.com', '2021-08-10 13:15:45', 0, '', 0),
(3, 1, 'testuser2', 'Test User2 Name', '$2y$10$lMfTw3PnDP0vxd3Ka/C/9upfArB8gqJDGaJoSqbR67Sjx6oBMDi4W', 'testuser2@gmail.com', '2021-08-10 13:16:11', 0, '', 0),
(4, 1, 'testuser3', 'Test User3 Name', '$2y$10$w26.8yTPNACAAVmNKW.Zie9hChBfjDTQv9Hs9HB9Z1lwvNFIM3BB2', 'testuser3@gmail.com', '2021-08-10 13:16:35', 0, '', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_variables`
--

CREATE TABLE `mrbs_variables` (
  `id` int(11) NOT NULL,
  `variable_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variable_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `mrbs_variables`
--

INSERT INTO `mrbs_variables` (`id`, `variable_name`, `variable_content`) VALUES
(1, 'db_version', '81'),
(2, 'local_db_version', '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `mrbs_zoneinfo`
--

CREATE TABLE `mrbs_zoneinfo` (
  `id` int(11) NOT NULL,
  `timezone` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `outlook_compatible` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `vtimezone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_updated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `mrbs_area`
--
ALTER TABLE `mrbs_area`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_area_name` (`area_name`);

--
-- Indici per le tabelle `mrbs_entry`
--
ALTER TABLE `mrbs_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `repeat_id` (`repeat_id`),
  ADD KEY `idxStartTime` (`start_time`),
  ADD KEY `idxEndTime` (`end_time`),
  ADD KEY `idxRoomStartEnd` (`room_id`,`start_time`,`end_time`);

--
-- Indici per le tabelle `mrbs_participants`
--
ALTER TABLE `mrbs_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_entryid_username` (`entry_id`,`username`);

--
-- Indici per le tabelle `mrbs_repeat`
--
ALTER TABLE `mrbs_repeat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`);

--
-- Indici per le tabelle `mrbs_room`
--
ALTER TABLE `mrbs_room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_room_name` (`area_id`,`room_name`),
  ADD KEY `idxSortKey` (`sort_key`);

--
-- Indici per le tabelle `mrbs_sessions`
--
ALTER TABLE `mrbs_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idxAccess` (`access`);

--
-- Indici per le tabelle `mrbs_users`
--
ALTER TABLE `mrbs_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_name` (`name`);

--
-- Indici per le tabelle `mrbs_variables`
--
ALTER TABLE `mrbs_variables`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_variable_name` (`variable_name`);

--
-- Indici per le tabelle `mrbs_zoneinfo`
--
ALTER TABLE `mrbs_zoneinfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_timezone` (`timezone`,`outlook_compatible`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `mrbs_area`
--
ALTER TABLE `mrbs_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `mrbs_entry`
--
ALTER TABLE `mrbs_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT per la tabella `mrbs_participants`
--
ALTER TABLE `mrbs_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `mrbs_repeat`
--
ALTER TABLE `mrbs_repeat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `mrbs_room`
--
ALTER TABLE `mrbs_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `mrbs_users`
--
ALTER TABLE `mrbs_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `mrbs_variables`
--
ALTER TABLE `mrbs_variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `mrbs_zoneinfo`
--
ALTER TABLE `mrbs_zoneinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `mrbs_entry`
--
ALTER TABLE `mrbs_entry`
  ADD CONSTRAINT `mrbs_entry_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `mrbs_room` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mrbs_entry_ibfk_2` FOREIGN KEY (`repeat_id`) REFERENCES `mrbs_repeat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `mrbs_participants`
--
ALTER TABLE `mrbs_participants`
  ADD CONSTRAINT `mrbs_participants_ibfk_1` FOREIGN KEY (`entry_id`) REFERENCES `mrbs_entry` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `mrbs_repeat`
--
ALTER TABLE `mrbs_repeat`
  ADD CONSTRAINT `mrbs_repeat_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `mrbs_room` (`id`) ON UPDATE CASCADE;

--
-- Limiti per la tabella `mrbs_room`
--
ALTER TABLE `mrbs_room`
  ADD CONSTRAINT `mrbs_room_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `mrbs_area` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
