-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mar 17, 2020 alle 11:19
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bugtracker_equals`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_api_token_table`
--

CREATE TABLE `mantis_api_token_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL,
  `hash` varchar(128) NOT NULL,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_used` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_api_token_table`
--

INSERT INTO `mantis_api_token_table` (`id`, `user_id`, `name`, `hash`, `date_created`, `date_used`) VALUES
(1, 1, 'Test Token 1', 'df01451179ffd934a687c73168f98a75fd36cb467e410eea15062cafe6289d3f', 1584006054, 1),
(2, 1, 'Test Token 2', 'f04d6c39d67a7f4fda231fa48b60e1bbc1f528d50d3a7933497dea760f74e954', 1584006066, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bugnote_table`
--

CREATE TABLE `mantis_bugnote_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reporter_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bugnote_text_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `note_type` int(11) DEFAULT 0,
  `note_attr` varchar(250) DEFAULT '',
  `time_tracking` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bugnote_table`
--

INSERT INTO `mantis_bugnote_table` (`id`, `bug_id`, `reporter_id`, `bugnote_text_id`, `view_state`, `note_type`, `note_attr`, `time_tracking`, `last_modified`, `date_submitted`) VALUES
(1, 17, 1, 1, 10, 0, '', 0, 1584006691, 1584006691),
(2, 7, 1, 2, 10, 0, '', 0, 1584006749, 1584006749),
(3, 8, 1, 3, 10, 0, '', 0, 1584006857, 1584006857),
(4, 13, 1, 4, 10, 0, '', 0, 1584006905, 1584006905);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bugnote_text_table`
--

CREATE TABLE `mantis_bugnote_text_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `note` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bugnote_text_table`
--

INSERT INTO `mantis_bugnote_text_table` (`id`, `note`) VALUES
(1, 'no bug note'),
(2, 'important issue'),
(3, 'ui problem'),
(4, 'tried on Ubuntu');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_file_table`
--

CREATE TABLE `mantis_bug_file_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_history_table`
--

CREATE TABLE `mantis_bug_history_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `field_name` varchar(64) NOT NULL,
  `old_value` varchar(255) NOT NULL,
  `new_value` varchar(255) NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT 0,
  `date_modified` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_history_table`
--

INSERT INTO `mantis_bug_history_table` (`id`, `user_id`, `bug_id`, `field_name`, `old_value`, `new_value`, `type`, `date_modified`) VALUES
(1, 1, 1, '', '', '', 1, 1584003480),
(2, 1, 1, 'date_submitted', '1584003480', '1205794800', 0, 1584003480),
(3, 1, 1, 'last_updated', '1584003480', '1557180000', 0, 1584003480),
(4, 1, 2, '', '', '', 1, 1584003481),
(5, 1, 2, 'date_submitted', '1584003481', '1119477600', 0, 1584003481),
(6, 1, 2, 'last_updated', '1584003481', '1557180000', 0, 1584003481),
(7, 1, 3, '', '', '', 1, 1584003481),
(8, 1, 3, 'date_submitted', '1584003481', '1557180000', 0, 1584003481),
(9, 1, 3, 'last_updated', '1584003481', '1557180000', 0, 1584003481),
(10, 1, 4, '', '', '', 1, 1584003482),
(11, 1, 4, 'handler_id', '0', '6', 0, 1584003482),
(12, 1, 4, 'date_submitted', '1584003482', '1557093600', 0, 1584003482),
(13, 1, 4, 'last_updated', '1584003482', '1557093600', 0, 1584003482),
(14, 1, 5, '', '', '', 1, 1584003482),
(15, 1, 5, 'date_submitted', '1584003482', '1557093600', 0, 1584003482),
(16, 1, 5, 'last_updated', '1584003482', '1557093600', 0, 1584003482),
(17, 1, 6, '', '', '', 1, 1584003483),
(18, 1, 6, 'date_submitted', '1584003483', '1441144800', 0, 1584003483),
(19, 1, 6, 'last_updated', '1584003483', '1557093600', 0, 1584003483),
(20, 1, 7, '', '', '', 1, 1584003483),
(21, 1, 7, 'handler_id', '0', '10', 0, 1584003483),
(22, 1, 7, 'date_submitted', '1584003483', '1404511200', 0, 1584003483),
(23, 1, 7, 'last_updated', '1584003483', '1557093600', 0, 1584003484),
(24, 1, 8, '', '', '', 1, 1584003484),
(25, 1, 8, 'handler_id', '0', '10', 0, 1584003484),
(26, 1, 8, 'date_submitted', '1584003484', '1554242400', 0, 1584003484),
(27, 1, 8, 'last_updated', '1584003484', '1557093600', 0, 1584003484),
(28, 1, 9, '', '', '', 1, 1584003484),
(29, 1, 9, 'handler_id', '0', '10', 0, 1584003485),
(30, 1, 9, 'date_submitted', '1584003484', '1303768800', 0, 1584003485),
(31, 1, 9, 'last_updated', '1584003485', '1557093600', 0, 1584003485),
(32, 1, 10, '', '', '', 1, 1584003485),
(33, 1, 10, 'handler_id', '0', '6', 0, 1584003486),
(34, 1, 10, 'date_submitted', '1584003485', '1557093600', 0, 1584003486),
(35, 1, 10, 'last_updated', '1584003486', '1557093600', 0, 1584003486),
(36, 1, 11, '', '', '', 1, 1584003486),
(37, 1, 11, 'date_submitted', '1584003486', '1556748000', 0, 1584003487),
(38, 1, 11, 'last_updated', '1584003487', '1557093600', 0, 1584003487),
(39, 1, 12, '', '', '', 1, 1584003488),
(40, 1, 12, 'handler_id', '0', '10', 0, 1584003488),
(41, 1, 12, 'date_submitted', '1584003488', '1557093600', 0, 1584003488),
(42, 1, 12, 'last_updated', '1584003488', '1557093600', 0, 1584003488),
(43, 1, 13, '', '', '', 1, 1584003488),
(44, 1, 13, 'date_submitted', '1584003488', '1556834400', 0, 1584003489),
(45, 1, 13, 'last_updated', '1584003489', '1557093600', 0, 1584003489),
(46, 1, 14, '', '', '', 1, 1584003489),
(47, 1, 14, 'handler_id', '0', '10', 0, 1584003489),
(48, 1, 14, 'date_submitted', '1584003489', '1556834400', 0, 1584003489),
(49, 1, 14, 'last_updated', '1584003489', '1557093600', 0, 1584003489),
(50, 1, 15, '', '', '', 1, 1584003489),
(51, 1, 15, 'handler_id', '0', '10', 0, 1584003489),
(52, 1, 15, 'date_submitted', '1584003489', '1556920800', 0, 1584003489),
(53, 1, 15, 'last_updated', '1584003489', '1557007200', 0, 1584003490),
(54, 1, 16, '', '', '', 1, 1584003490),
(55, 1, 16, 'handler_id', '0', '10', 0, 1584003490),
(56, 1, 16, 'date_submitted', '1584003490', '1556575200', 0, 1584003490),
(57, 1, 16, 'last_updated', '1584003490', '1556834400', 0, 1584003491),
(58, 1, 17, '', '', '', 1, 1584003491),
(59, 1, 17, 'handler_id', '0', '8', 0, 1584003491),
(60, 1, 17, 'date_submitted', '1584003491', '1556488800', 0, 1584003491),
(61, 1, 17, 'last_updated', '1584003491', '1556575200', 0, 1584003491),
(62, 1, 3, '', '1', '', 12, 1584006190),
(63, 1, 3, '', '6', '', 12, 1584006202),
(64, 1, 1, '', '10', '', 12, 1584006224),
(65, 1, 5, '', '3', '', 12, 1584006237),
(66, 1, 5, '', '1', '', 12, 1584006266),
(67, 1, 9, '', '7', '', 12, 1584006276),
(68, 1, 9, '', '6', '', 12, 1584006280),
(69, 1, 9, '', '1', '', 12, 1584006285),
(70, 1, 17, '', '2', '', 12, 1584006299),
(71, 1, 17, '', '10', '', 12, 1584006316),
(72, 1, 17, '', '1', '9', 18, 1584006351),
(73, 1, 9, '', '1', '17', 18, 1584006351),
(74, 1, 17, '', '3', '5', 18, 1584006356),
(75, 1, 5, '', '2', '17', 18, 1584006356),
(76, 1, 17, '', '0', '1', 18, 1584006362),
(77, 1, 1, '', '4', '17', 18, 1584006362),
(78, 1, 3, '', '1', '1', 18, 1584006389),
(79, 1, 1, '', '1', '3', 18, 1584006389),
(80, 1, 3, '', '1', '10', 18, 1584006394),
(81, 1, 10, '', '1', '3', 18, 1584006394),
(82, 1, 12, '', '4', '4', 18, 1584006404),
(83, 1, 4, '', '0', '12', 18, 1584006404),
(84, 1, 15, '', '1', '16', 18, 1584006414),
(85, 1, 16, '', '1', '15', 18, 1584006414),
(86, 1, 15, '', '1', '1', 18, 1584006417),
(87, 1, 1, '', '1', '15', 18, 1584006417),
(88, 1, 15, '', '1', '3', 18, 1584006432),
(89, 1, 3, '', '1', '15', 18, 1584006432),
(90, 1, 15, '', '1', '8', 18, 1584006434),
(91, 1, 8, '', '1', '15', 18, 1584006434),
(92, 1, 17, '', 'administration', '', 25, 1584006497),
(93, 1, 17, '', 'api', '', 25, 1584006497),
(94, 1, 9, '', 'administration', '', 25, 1584006525),
(95, 1, 9, '', 'api', '', 25, 1584006525),
(96, 1, 9, '', 'code', '', 25, 1584006525),
(97, 1, 10, '', 'code', '', 25, 1584006543),
(98, 1, 10, '', 'date', '', 25, 1584006543),
(99, 1, 10, '', 'download', '', 25, 1584006543),
(100, 1, 13, '', 'api', '', 25, 1584006557),
(101, 1, 13, '', 'faq', '', 25, 1584006557),
(102, 1, 13, '', 'html', '', 25, 1584006557),
(103, 1, 8, '', 'ui', '', 25, 1584006579),
(104, 1, 8, '', 'graphics', '', 25, 1584006579),
(105, 1, 7, '', 'filter', '', 25, 1584006585),
(106, 1, 17, '', '0000001', '', 2, 1584006691),
(107, 1, 7, '', '0000002', '', 2, 1584006750),
(108, 1, 8, '', '0000003', '', 2, 1584006857),
(109, 1, 13, '', '0000004', '', 2, 1584006905),
(110, 1, 10, '', '0000005', '', 2, 1584006956),
(111, 1, 9, '', '0000006', '', 2, 1584007011),
(112, 1, 15, '', '0000007', '', 2, 1584007073),
(113, 1, 3, '', '0000008', '', 2, 1584007138);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_monitor_table`
--

CREATE TABLE `mantis_bug_monitor_table` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_monitor_table`
--

INSERT INTO `mantis_bug_monitor_table` (`user_id`, `bug_id`) VALUES
(1, 3),
(1, 5),
(1, 9),
(2, 17),
(3, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_relationship_table`
--

CREATE TABLE `mantis_bug_relationship_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `source_bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `destination_bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `relationship_type` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_relationship_table`
--

INSERT INTO `mantis_bug_relationship_table` (`id`, `source_bug_id`, `destination_bug_id`, `relationship_type`) VALUES
(1, 17, 9, 1),
(2, 5, 17, 2),
(3, 17, 1, 0),
(4, 3, 1, 1),
(5, 3, 10, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_revision_table`
--

CREATE TABLE `mantis_bug_revision_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `bug_id` int(10) UNSIGNED NOT NULL,
  `bugnote_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_table`
--

CREATE TABLE `mantis_bug_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reporter_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `handler_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `duplicate_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `priority` smallint(6) NOT NULL DEFAULT 30,
  `severity` smallint(6) NOT NULL DEFAULT 50,
  `reproducibility` smallint(6) NOT NULL DEFAULT 10,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `resolution` smallint(6) NOT NULL DEFAULT 10,
  `projection` smallint(6) NOT NULL DEFAULT 10,
  `eta` smallint(6) NOT NULL DEFAULT 10,
  `bug_text_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `platform` varchar(32) NOT NULL DEFAULT '',
  `version` varchar(64) NOT NULL DEFAULT '',
  `fixed_in_version` varchar(64) NOT NULL DEFAULT '',
  `build` varchar(32) NOT NULL DEFAULT '',
  `profile_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `summary` varchar(128) NOT NULL DEFAULT '',
  `sponsorship_total` int(11) NOT NULL DEFAULT 0,
  `sticky` tinyint(4) NOT NULL DEFAULT 0,
  `target_version` varchar(64) NOT NULL DEFAULT '',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `due_date` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `last_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_table`
--

INSERT INTO `mantis_bug_table` (`id`, `project_id`, `reporter_id`, `handler_id`, `duplicate_id`, `priority`, `severity`, `reproducibility`, `status`, `resolution`, `projection`, `eta`, `bug_text_id`, `os`, `os_build`, `platform`, `version`, `fixed_in_version`, `build`, `profile_id`, `view_state`, `summary`, `sponsorship_total`, `sticky`, `target_version`, `category_id`, `date_submitted`, `due_date`, `last_updated`) VALUES
(1, 1, 2, 0, 0, 30, 50, 70, 30, 10, 10, 10, 1, '', '', '', '1.1.1', '', '', 0, 10, 'Set default product version', 0, 0, '', 1, 1205794800, 1, 1584006417),
(2, 1, 3, 0, 0, 30, 50, 70, 10, 10, 10, 10, 2, '', '', '', '1.0.0a3', '', '', 0, 10, 'Reports to require admin approval before appearing \'live\'', 0, 0, '', 1, 1119477600, 1, 1557180000),
(3, 1, 4, 0, 0, 30, 50, 70, 10, 10, 10, 10, 3, '', '', '', '', '', '', 0, 10, 'Add column in excel time tracking report', 0, 0, '', 1, 1557180000, 1, 1584007138),
(4, 1, 5, 6, 0, 30, 50, 70, 80, 0, 10, 10, 4, '', '', '', '2.21.0', '', '', 0, 10, 'Invalid JSON response from server after attachment is submitted', 0, 0, '', 1, 1557093600, 1, 1584006405),
(5, 1, 7, 0, 0, 30, 50, 70, 10, 10, 10, 10, 5, '', '', '', '2.21.0', '', '', 0, 10, 'In the page \"View Issues\" in \"rtl\" view mode', 0, 0, '', 1, 1557093600, 1, 1584006356),
(6, 1, 8, 0, 0, 30, 50, 70, 10, 10, 10, 10, 6, '', '', '', '1.2.19', '', '', 0, 10, 'lang_exists should account for plugin lang strings', 0, 0, '', 1, 1441144800, 1, 1557093600),
(7, 1, 9, 10, 0, 30, 50, 70, 50, 10, 10, 10, 7, '', '', '', '1.2.17', '', '', 0, 10, 'Validate plugin folder name and name match during setup', 0, 0, '', 1, 1404511200, 1, 1584006749),
(8, 1, 11, 10, 0, 30, 50, 70, 80, 0, 10, 10, 8, '', '', '', '2.20.0', '', '', 0, 10, 'APPLICATION ERROR #11', 0, 0, '', 1, 1554242400, 1, 1584006857);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_tag_table`
--

CREATE TABLE `mantis_bug_tag_table` (
  `bug_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tag_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `date_attached` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_tag_table`
--

INSERT INTO `mantis_bug_tag_table` (`bug_id`, `tag_id`, `user_id`, `date_attached`) VALUES
(7, 10, 1, 1584006585),
(8, 8, 1, 1584006579),
(8, 9, 1, 1584006579),
(9, 1, 1, 1584006525),
(9, 2, 1, 1584006525),
(9, 3, 1, 1584006525),
(10, 3, 1, 1584006543);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_bug_text_table`
--

CREATE TABLE `mantis_bug_text_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext NOT NULL,
  `steps_to_reproduce` longtext NOT NULL,
  `additional_information` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_bug_text_table`
--

INSERT INTO `mantis_bug_text_table` (`id`, `description`, `steps_to_reproduce`, `additional_information`) VALUES
(1, 'Set default product version', '', ''),
(2, 'Reports to require admin approval before appearing \'live\'', '', ''),
(3, 'Add column in excel time tracking report', '', ''),
(4, 'Invalid JSON response from server after attachment is submitted', '', ''),
(5, 'In the page \"View Issues\" in \"rtl\" view mode', '', ''),
(6, 'lang_exists should account for plugin lang strings', '', ''),
(7, 'Validate plugin folder name and name match during setup', '', ''),
(8, 'APPLICATION ERROR #11', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_category_table`
--

CREATE TABLE `mantis_category_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_category_table`
--

INSERT INTO `mantis_category_table` (`id`, `project_id`, `user_id`, `name`, `status`) VALUES
(1, 0, 0, 'General', 0),
(2, 0, 0, 'administration', 0),
(3, 0, 0, 'attachments', 0),
(4, 0, 0, 'bugtracker', 0),
(5, 0, 0, 'db schema', 0),
(6, 0, 0, 'documentation', 0),
(7, 0, 0, 'feature', 0),
(8, 0, 0, 'installation', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_config_table`
--

CREATE TABLE `mantis_config_table` (
  `config_id` varchar(64) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `access_reqd` int(11) DEFAULT 0,
  `type` int(11) DEFAULT 90,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_config_table`
--

INSERT INTO `mantis_config_table` (`config_id`, `project_id`, `user_id`, `access_reqd`, `type`, `value`) VALUES
('database_version', 0, 0, 90, 1, '209'),
('plugin_Csv_import_schema', 0, 0, 90, 1, '-1');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_custom_field_project_table`
--

CREATE TABLE `mantis_custom_field_project_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sequence` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_custom_field_project_table`
--

INSERT INTO `mantis_custom_field_project_table` (`field_id`, `project_id`, `sequence`) VALUES
(1, 1, 0),
(1, 5, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_custom_field_string_table`
--

CREATE TABLE `mantis_custom_field_string_table` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `value` varchar(255) NOT NULL DEFAULT '',
  `text` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_custom_field_table`
--

CREATE TABLE `mantis_custom_field_table` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT 0,
  `possible_values` text DEFAULT NULL,
  `default_value` varchar(255) NOT NULL DEFAULT '',
  `valid_regexp` varchar(255) NOT NULL DEFAULT '',
  `access_level_r` smallint(6) NOT NULL DEFAULT 0,
  `access_level_rw` smallint(6) NOT NULL DEFAULT 0,
  `length_min` int(11) NOT NULL DEFAULT 0,
  `length_max` int(11) NOT NULL DEFAULT 0,
  `require_report` tinyint(4) NOT NULL DEFAULT 0,
  `require_update` tinyint(4) NOT NULL DEFAULT 0,
  `display_report` tinyint(4) NOT NULL DEFAULT 0,
  `display_update` tinyint(4) NOT NULL DEFAULT 1,
  `require_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `display_closed` tinyint(4) NOT NULL DEFAULT 0,
  `require_closed` tinyint(4) NOT NULL DEFAULT 0,
  `filter_by` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_custom_field_table`
--

INSERT INTO `mantis_custom_field_table` (`id`, `name`, `type`, `possible_values`, `default_value`, `valid_regexp`, `access_level_r`, `access_level_rw`, `length_min`, `length_max`, `require_report`, `require_update`, `display_report`, `display_update`, `require_resolved`, `display_resolved`, `display_closed`, `require_closed`, `filter_by`) VALUES
(1, 'Custom Field 1', 0, 'aaa | bbb | ccccc', 'aaa', '', 10, 10, 3, 5, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(2, 'Custom Field 2', 0, '', '', '', 10, 10, 1, 7, 1, 0, 0, 1, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_email_table`
--

CREATE TABLE `mantis_email_table` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(64) NOT NULL DEFAULT '',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `metadata` longtext NOT NULL,
  `body` longtext NOT NULL,
  `submitted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_email_table`
--

INSERT INTO `mantis_email_table` (`email_id`, `email`, `subject`, `metadata`, `body`, `submitted`) VALUES
(1, '74b37f106646a5ddb501ba5128c1028a@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"vipart\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=2&confirm_hash=IYbtrLMPfDrS6anLWXQvuwNUgwNUYZCx_Qjb978rymOrvxXu1yvuCGvdEkqARR06K-73s8EN2S06AQ-L3vTq\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003480),
(2, '4d9a582c8d228f5ac45a6476b8d5d8de@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"richbrit\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=3&confirm_hash=GWL6QM2TJ_Y_Blv8jac3R6fYIr698QRjN90mrdX6XqLe8Wt9HJo5sSynXUpxhDYjpdXM7DndwkQYq9CLfDRe\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003481),
(3, '5b426f6f57aeed1d9c1906c18139b59c@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"khojda\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=4&confirm_hash=hsaYj8JT4IkAaLHh3iOfHFOy8Ouayqhk8SAEc9Fc3J1aGjnl8s4W68urkzHMzZO5I-H1Bxx3DYKAEFH9aGTV\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003481),
(4, '7896af9623fc21ba7d2e8b2e8508ca3b@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"bobheck11\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=5&confirm_hash=3CRu_5jdG_4TSZKEP-D5GGHVOpEzo2ILiRQKt1JFQhL_-4hUdXO8QN0roucNiaZ5kILbTvGCYLLYXC3WfrgE\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003481),
(5, 'd1323a99947edca9476f60dc869ea44b@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"atrol\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=6&confirm_hash=s-Fu_kb2HDRtjl5Rg6JbAo6a_U3uN_CKRTT8Uqiw0qlRxxcjX4ONCB8j26epJVpEDFgCUHdLe67OVdaUBn-M\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003482),
(6, '598af87762c067d7d27711bcf583c685@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"sjazz\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=7&confirm_hash=_f_hDO1riYyI0zGJD2Ud2DfcZQetUnQrsWQyLuPZcI9PQtVHKyjgVVDXcTvxmWgcGtDi5gjwEUqFZp-ilDrg\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003482),
(7, '0fc438ace24c510accace9bbadb07a40@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"cproensa\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=8&confirm_hash=W_7ASDgnC0elY0YMvvyNQWOt1_ojBgH_EGI3jgGaFksf-NR5fZ_GUmk9gy9UrnyKla-MTuwDMMeeGbncOdQA\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003483),
(8, '1d71fcee6b61aff36a63dcdf7459ef39@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"aavagyan\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=9&confirm_hash=5mfV0_uhylV2btvuoqWPz2_XuTz8LnMFBUQQlTwRitHmlYx9-leJyaaTohHodeSU5O7E9xPCFBwuRir08tWU\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003483),
(9, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"dregad\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=10&confirm_hash=HaPseTVTLlyMEDQw4EhhDOKuKDfILdF_zJnAdSfhdpGszNYh8QtSjxnlBt-hxmzMh8vJgQIjqUBIklNSvMGl\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003483),
(10, '54bc575d0f3c8bb07521c04597cadb6a@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"syedjh_s\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=11&confirm_hash=HfCb_kpM6v-ebjdig2xuDRvou4NLtLaUfq_OSk02kl4HCVbZo45FzPdOBM_G7LxHLyKGn-i5_cmGritBA4aV\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003484),
(11, '539d1cb329f3297a366e27437dc8ef86@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"Lapinkiller\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=12&confirm_hash=KDDs-cRgCmMgpLrgUlgqO1n7XlXKZj1J1F3HIqnIC6Nmi6tQ7tMoF5kKzhEPcjC0800m_y_h6hePjP4ORxLP\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003484),
(12, 'a8b0fc83eda6e2afa39533920529696b@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"immae\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=13&confirm_hash=jXnDMkoeCfHKCjwXQmORmEjDOOsH0kK3afjq9EMFCAMOpJZSIXxNqdMIXCX0u8W5D0QUKd66GWfP7EuM1G3x\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003485),
(13, '298a5c65bc23d26c1aea733d52796f91@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"camila luz\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=14&confirm_hash=srg99_OpouPfXvFskdVHNNaqrf_Au-UWJUqe08GLeG790iLKLgOgFQ9GCp-oIydnRzsDPtEva8tLGwIKJucH\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003486),
(14, '509175904f58e2a9bbe186e0e3eacbd9@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"silju\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=15&confirm_hash=vpFAeP8Sh02v1gTh_q7pjb53_2Nl7HKxwv7doZxMVZqOhARET26mTm4tePSv40eTumAunqs9U9h3xhBpt5mH\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003488),
(15, 'c5be89d316fa1b866d889363a5076b4c@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"kskr19\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=16&confirm_hash=k9SUnKjMLk1F1ozmuqteL_eGh59lnXnomHtUsmr9-Ze_udM1LVJpxpX8cPHF_hQzxdlrFMXcDu0nSzpX6UHM\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003490),
(16, '4a06be466b762e47abc95873e954c0b8@localhost', '[MantisBT] Nuovo account registrato', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'Benvenuto e grazie per esserti registrato.  È stato creato con successo il tuo account con il nome utente \"fubertosi\".  Per completare la registrazione, visita il seguente indirizzo (assicurati di inserirlo in una singola riga) e imposta la tua password di accesso:\n\nhttp://localhost/mantisbt/verify.php?id=17&confirm_hash=BCSO7R0tQiDCXsIkwJKaNvML480S-mPJ46kMFSA-zf0peZWho4bUvMnIQuuDHcHUyVvWLAZ9q68TDTiqKJ0e\n\nSe la richiesta di iscrizione non è stata fatta da te, ignora semplicemente questo messaggio.\n\nNON RISPONDERE A QUESTO MESSAGGIO', 1584003491),
(17, '4a06be466b762e47abc95873e954c0b8@localhost', '[Test Project 0000017]: Wrong access_level settings when updating rights in the project admin page', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000001) administrator (administrator) - 2020-03-12 10:51\n http://localhost/mantisbt/view.php?id=17#c1 \n---------------------------------------------------------------------- \nno bug note\n----------------------------------------------------------------------', 1584006691),
(18, '0fc438ace24c510accace9bbadb07a40@localhost', '[Test Project 0000017]: Wrong access_level settings when updating rights in the project admin page', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000001) administrator (administrator) - 2020-03-12 10:51\n http://localhost/mantisbt/view.php?id=17#c1 \n---------------------------------------------------------------------- \nno bug note\n----------------------------------------------------------------------', 1584006691),
(19, '74b37f106646a5ddb501ba5128c1028a@localhost', '[Test Project 0000017]: Wrong access_level settings when updating rights in the project admin page', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000001) administrator (administrator) - 2020-03-12 10:51\n http://localhost/mantisbt/view.php?id=17#c1 \n---------------------------------------------------------------------- \nno bug note\n----------------------------------------------------------------------', 1584006691),
(20, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[Test Project 0000017]: Wrong access_level settings when updating rights in the project admin page', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000001) administrator (administrator) - 2020-03-12 10:51\n http://localhost/mantisbt/view.php?id=17#c1 \n---------------------------------------------------------------------- \nno bug note\n----------------------------------------------------------------------', 1584006691),
(21, '1d71fcee6b61aff36a63dcdf7459ef39@localhost', '[Test Project 0000007]: Validate plugin folder name and name match during setup', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000002) administrator (administrator) - 2020-03-12 10:52\n http://localhost/mantisbt/view.php?id=7#c2 \n---------------------------------------------------------------------- \nimportant issue\n----------------------------------------------------------------------', 1584006750),
(22, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[Test Project 0000007]: Validate plugin folder name and name match during setup', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000002) administrator (administrator) - 2020-03-12 10:52\n http://localhost/mantisbt/view.php?id=7#c2 \n---------------------------------------------------------------------- \nimportant issue\n----------------------------------------------------------------------', 1584006750),
(23, '54bc575d0f3c8bb07521c04597cadb6a@localhost', '[Test Project 0000008]: APPLICATION ERROR #11', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000003) administrator (administrator) - 2020-03-12 10:54\n http://localhost/mantisbt/view.php?id=8#c3 \n---------------------------------------------------------------------- \nui problem\n----------------------------------------------------------------------', 1584006857),
(24, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[Test Project 0000008]: APPLICATION ERROR #11', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000003) administrator (administrator) - 2020-03-12 10:54\n http://localhost/mantisbt/view.php?id=8#c3 \n---------------------------------------------------------------------- \nui problem\n----------------------------------------------------------------------', 1584006857),
(25, '509175904f58e2a9bbe186e0e3eacbd9@localhost', '[Test Project 0000013]: User password activation link showing as invalid', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000004) administrator (administrator) - 2020-03-12 10:55\n http://localhost/mantisbt/view.php?id=13#c4 \n---------------------------------------------------------------------- \ntried on Ubuntu\n----------------------------------------------------------------------', 1584006906),
(26, 'a8b0fc83eda6e2afa39533920529696b@localhost', '[Test Project 0000010]: Impossible to use postgresql after upgrade', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000005) administrator (administrator) - 2020-03-12 10:55\n http://localhost/mantisbt/view.php?id=10#c5 \n---------------------------------------------------------------------- \nthis is not a real bug\n----------------------------------------------------------------------', 1584006956),
(27, 'd1323a99947edca9476f60dc869ea44b@localhost', '[Test Project 0000010]: Impossible to use postgresql after upgrade', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000005) administrator (administrator) - 2020-03-12 10:55\n http://localhost/mantisbt/view.php?id=10#c5 \n---------------------------------------------------------------------- \nthis is not a real bug\n----------------------------------------------------------------------', 1584006956),
(28, '539d1cb329f3297a366e27437dc8ef86@localhost', '[Test Project 0000009]: Plugin_force_uninstall isnot declared', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000006) administrator (administrator) - 2020-03-12 10:56\n http://localhost/mantisbt/view.php?id=9#c6 \n---------------------------------------------------------------------- \nchange request\n----------------------------------------------------------------------', 1584007011),
(29, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[Test Project 0000009]: Plugin_force_uninstall isnot declared', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000006) administrator (administrator) - 2020-03-12 10:56\n http://localhost/mantisbt/view.php?id=9#c6 \n---------------------------------------------------------------------- \nchange request\n----------------------------------------------------------------------', 1584007011),
(30, 'd1323a99947edca9476f60dc869ea44b@localhost', '[Test Project 0000009]: Plugin_force_uninstall isnot declared', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000006) administrator (administrator) - 2020-03-12 10:56\n http://localhost/mantisbt/view.php?id=9#c6 \n---------------------------------------------------------------------- \nchange request\n----------------------------------------------------------------------', 1584007011),
(31, '598af87762c067d7d27711bcf583c685@localhost', '[Test Project 0000009]: Plugin_force_uninstall isnot declared', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000006) administrator (administrator) - 2020-03-12 10:56\n http://localhost/mantisbt/view.php?id=9#c6 \n---------------------------------------------------------------------- \nchange request\n----------------------------------------------------------------------', 1584007011),
(32, 'f17a04d6b10fd50f4fb8a1b9bd8783c2@localhost', '[Test Project 0000015]: LOGFILE_NOT_WRITABLE error triggered if file does not exist', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000007) administrator (administrator) - 2020-03-12 10:57\n http://localhost/mantisbt/view.php?id=15#c7 \n---------------------------------------------------------------------- \ncan you help?\n----------------------------------------------------------------------', 1584007073),
(33, '5b426f6f57aeed1d9c1906c18139b59c@localhost', '[Test Project 0000003]: Add column in excel time tracking report', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000008) administrator (administrator) - 2020-03-12 10:58\n http://localhost/mantisbt/view.php?id=3#c8 \n---------------------------------------------------------------------- \nno bug note sorry\n----------------------------------------------------------------------', 1584007138),
(34, 'd1323a99947edca9476f60dc869ea44b@localhost', '[Test Project 0000003]: Add column in excel time tracking report', 'a:3:{s:7:\"headers\";a:0:{}s:7:\"charset\";s:5:\"utf-8\";s:8:\"hostname\";s:9:\"localhost\";}', 'A NOTE has been added to this issue.\n\n---------------------------------------------------------------------- \n (0000008) administrator (administrator) - 2020-03-12 10:58\n http://localhost/mantisbt/view.php?id=3#c8 \n---------------------------------------------------------------------- \nno bug note sorry\n----------------------------------------------------------------------', 1584007138);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_filters_table`
--

CREATE TABLE `mantis_filters_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL DEFAULT 0,
  `is_public` tinyint(4) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `filter_string` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_filters_table`
--

INSERT INTO `mantis_filters_table` (`id`, `user_id`, `project_id`, `is_public`, `name`, `filter_string`) VALUES
(1, 1, 1, 0, 'filter', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"03\",\"end_month\":\"03\",\"start_day\":1,\"end_day\":\"12\",\"start_year\":\"2020\",\"end_year\":\"2020\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"03\",\"last_updated_end_month\":\"03\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"12\",\"last_updated_start_year\":\"2020\",\"last_updated_end_year\":\"2020\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":{\"1\":[\"0\"],\"2\":[\"0\"],\"3\":[\"0\"],\"4\":[\"0\"],\"5\":[\"0\"]}}'),
(3, 1, 0, 1, 'search issues', 'v9#{\"_version\":\"v9\",\"_view_type\":\"simple\",\"category_id\":[\"0\"],\"severity\":[0],\"status\":[0],\"highlight_changed\":6,\"reporter_id\":[0],\"handler_id\":[0],\"project_id\":[-3],\"resolution\":[0],\"build\":[\"0\"],\"version\":[\"0\"],\"hide_status\":[90],\"monitor_user_id\":[0],\"sort\":\"last_updated\",\"dir\":\"DESC\",\"per_page\":50,\"match_type\":0,\"platform\":[\"0\"],\"os\":[\"0\"],\"os_build\":[\"0\"],\"fixed_in_version\":[\"0\"],\"target_version\":[\"0\"],\"profile_id\":[0],\"priority\":[0],\"note_user_id\":[0],\"sticky\":true,\"filter_by_date\":false,\"start_month\":\"03\",\"end_month\":\"03\",\"start_day\":1,\"end_day\":\"12\",\"start_year\":\"2020\",\"end_year\":\"2020\",\"filter_by_last_updated_date\":false,\"last_updated_start_month\":\"03\",\"last_updated_end_month\":\"03\",\"last_updated_start_day\":1,\"last_updated_end_day\":\"12\",\"last_updated_start_year\":\"2020\",\"last_updated_end_year\":\"2020\",\"search\":\"\",\"view_state\":0,\"tag_string\":\"\",\"tag_select\":0,\"relationship_type\":-1,\"relationship_bug\":0,\"custom_fields\":{\"1\":[\"0\"],\"2\":[\"0\"],\"3\":[\"0\"],\"4\":[\"0\"],\"5\":[\"0\"]}}');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_login_table`
--

CREATE TABLE `mantis_login_table` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_login_table`
--

INSERT INTO `mantis_login_table` (`username`, `password`) VALUES
('administrator', 'root1');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_news_table`
--

CREATE TABLE `mantis_news_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `poster_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `announcement` tinyint(4) NOT NULL DEFAULT 0,
  `headline` varchar(64) NOT NULL DEFAULT '',
  `body` longtext NOT NULL,
  `last_modified` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_posted` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_plugin_table`
--

CREATE TABLE `mantis_plugin_table` (
  `basename` varchar(40) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `priority` int(10) UNSIGNED NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_plugin_table`
--

INSERT INTO `mantis_plugin_table` (`basename`, `enabled`, `protected`, `priority`) VALUES
('Csv_import', 1, 0, 3),
('MantisCoreFormatting', 1, 0, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_project_file_table`
--

CREATE TABLE `mantis_project_file_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `diskfile` varchar(250) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `folder` varchar(250) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT 0,
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `content` longblob DEFAULT NULL,
  `date_added` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_project_hierarchy_table`
--

CREATE TABLE `mantis_project_hierarchy_table` (
  `child_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `inherit_parent` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_project_hierarchy_table`
--

INSERT INTO `mantis_project_hierarchy_table` (`child_id`, `parent_id`, `inherit_parent`) VALUES
(3, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_project_table`
--

CREATE TABLE `mantis_project_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` smallint(6) NOT NULL DEFAULT 10,
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `view_state` smallint(6) NOT NULL DEFAULT 10,
  `access_min` smallint(6) NOT NULL DEFAULT 10,
  `file_path` varchar(250) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `inherit_global` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_project_table`
--

INSERT INTO `mantis_project_table` (`id`, `name`, `status`, `enabled`, `view_state`, `access_min`, `file_path`, `description`, `category_id`, `inherit_global`) VALUES
(1, 'Test Project', 10, 1, 10, 10, '', '', 1, 1),
(2, 'Bicocca Project', 10, 1, 10, 10, '', 'using Tosca to test Mantis BT', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_project_user_list_table`
--

CREATE TABLE `mantis_project_user_list_table` (
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_project_user_list_table`
--

INSERT INTO `mantis_project_user_list_table` (`project_id`, `user_id`, `access_level`) VALUES
(1, 7, 25),
(1, 10, 25),
(1, 11, 25),
(1, 16, 25);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_project_version_table`
--

CREATE TABLE `mantis_project_version_table` (
  `id` int(11) NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(64) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `released` tinyint(4) NOT NULL DEFAULT 1,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0,
  `date_order` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_project_version_table`
--

INSERT INTO `mantis_project_version_table` (`id`, `project_id`, `version`, `description`, `released`, `obsolete`, `date_order`) VALUES
(1, 5, '3', '', 0, 0, 1584008099);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_sponsorship_table`
--

CREATE TABLE `mantis_sponsorship_table` (
  `id` int(11) NOT NULL,
  `bug_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `amount` int(11) NOT NULL DEFAULT 0,
  `logo` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT '',
  `paid` tinyint(4) NOT NULL DEFAULT 0,
  `date_submitted` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `last_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_tag_table`
--

CREATE TABLE `mantis_tag_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_updated` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_tag_table`
--

INSERT INTO `mantis_tag_table` (`id`, `user_id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 1, 'administration', '', 1584006497, 1584006497),
(2, 1, 'api', '', 1584006497, 1584006497),
(3, 1, 'code', '', 1584006525, 1584006525),
(4, 1, 'date', '', 1584006543, 1584006543),
(5, 1, 'download', '', 1584006543, 1584006543),
(6, 1, 'faq', '', 1584006557, 1584006557),
(7, 1, 'html', '', 1584006557, 1584006557);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_tokens_table`
--

CREATE TABLE `mantis_tokens_table` (
  `id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `value` longtext NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `expiry` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_tokens_table`
--

INSERT INTO `mantis_tokens_table` (`id`, `owner`, `type`, `value`, `timestamp`, `expiry`) VALUES
(2, 1, 5, '{\"filter\":false,\"profile\":true}', 1584002994, 1615544506),
(3, 2, 7, 'IYbtrLMPfDrS6anLWXQvuwNUgwNUYZCx_Qjb978rymOrvxXu1yvuCGvdEkqARR06K-73s8EN2S06AQ-L3vTq', 1584003480, 1584608280),
(4, 3, 7, 'GWL6QM2TJ_Y_Blv8jac3R6fYIr698QRjN90mrdX6XqLe8Wt9HJo5sSynXUpxhDYjpdXM7DndwkQYq9CLfDRe', 1584003480, 1584608280),
(5, 4, 7, 'hsaYj8JT4IkAaLHh3iOfHFOy8Ouayqhk8SAEc9Fc3J1aGjnl8s4W68urkzHMzZO5I-H1Bxx3DYKAEFH9aGTV', 1584003481, 1584608281),
(6, 5, 7, '3CRu_5jdG_4TSZKEP-D5GGHVOpEzo2ILiRQKt1JFQhL_-4hUdXO8QN0roucNiaZ5kILbTvGCYLLYXC3WfrgE', 1584003481, 1584608281),
(7, 6, 7, 's-Fu_kb2HDRtjl5Rg6JbAo6a_U3uN_CKRTT8Uqiw0qlRxxcjX4ONCB8j26epJVpEDFgCUHdLe67OVdaUBn-M', 1584003482, 1584608282),
(8, 7, 7, '_f_hDO1riYyI0zGJD2Ud2DfcZQetUnQrsWQyLuPZcI9PQtVHKyjgVVDXcTvxmWgcGtDi5gjwEUqFZp-ilDrg', 1584003482, 1584608282),
(9, 8, 7, 'W_7ASDgnC0elY0YMvvyNQWOt1_ojBgH_EGI3jgGaFksf-NR5fZ_GUmk9gy9UrnyKla-MTuwDMMeeGbncOdQA', 1584003483, 1584608283),
(10, 9, 7, '5mfV0_uhylV2btvuoqWPz2_XuTz8LnMFBUQQlTwRitHmlYx9-leJyaaTohHodeSU5O7E9xPCFBwuRir08tWU', 1584003483, 1584608283),
(11, 10, 7, 'HaPseTVTLlyMEDQw4EhhDOKuKDfILdF_zJnAdSfhdpGszNYh8QtSjxnlBt-hxmzMh8vJgQIjqUBIklNSvMGl', 1584003483, 1584608283),
(12, 11, 7, 'HfCb_kpM6v-ebjdig2xuDRvou4NLtLaUfq_OSk02kl4HCVbZo45FzPdOBM_G7LxHLyKGn-i5_cmGritBA4aV', 1584003484, 1584608284),
(13, 12, 7, 'KDDs-cRgCmMgpLrgUlgqO1n7XlXKZj1J1F3HIqnIC6Nmi6tQ7tMoF5kKzhEPcjC0800m_y_h6hePjP4ORxLP', 1584003484, 1584608284),
(14, 13, 7, 'jXnDMkoeCfHKCjwXQmORmEjDOOsH0kK3afjq9EMFCAMOpJZSIXxNqdMIXCX0u8W5D0QUKd66GWfP7EuM1G3x', 1584003485, 1584608285),
(15, 14, 7, 'srg99_OpouPfXvFskdVHNNaqrf_Au-UWJUqe08GLeG790iLKLgOgFQ9GCp-oIydnRzsDPtEva8tLGwIKJucH', 1584003486, 1584608286),
(16, 15, 7, 'vpFAeP8Sh02v1gTh_q7pjb53_2Nl7HKxwv7doZxMVZqOhARET26mTm4tePSv40eTumAunqs9U9h3xhBpt5mH', 1584003488, 1584608288),
(17, 16, 7, 'k9SUnKjMLk1F1ozmuqteL_eGh59lnXnomHtUsmr9-Ze_udM1LVJpxpX8cPHF_hQzxdlrFMXcDu0nSzpX6UHM', 1584003490, 1584608290),
(18, 17, 7, 'BCSO7R0tQiDCXsIkwJKaNvML480S-mPJ46kMFSA-zf0peZWho4bUvMnIQuuDHcHUyVvWLAZ9q68TDTiqKJ0e', 1584003491, 1584608291),
(21, 1, 3, '3,15,9,10,13', 1584006116, 1584093607),
(23, 1, 4, '1', 1584007989, 1584008806);

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_user_pref_table`
--

CREATE TABLE `mantis_user_pref_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `project_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `default_profile` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `default_project` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `refresh_delay` int(11) NOT NULL DEFAULT 0,
  `redirect_delay` int(11) NOT NULL DEFAULT 0,
  `bugnote_order` varchar(4) NOT NULL DEFAULT 'ASC',
  `email_on_new` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_assigned` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_feedback` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_resolved` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_closed` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_reopened` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_bugnote` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_status` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority` tinyint(4) NOT NULL DEFAULT 0,
  `email_on_priority_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_status_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_bugnote_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_reopened_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_closed_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_resolved_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_feedback_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_assigned_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_on_new_min_severity` smallint(6) NOT NULL DEFAULT 10,
  `email_bugnote_limit` smallint(6) NOT NULL DEFAULT 0,
  `language` varchar(32) NOT NULL DEFAULT 'english',
  `timezone` varchar(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_user_pref_table`
--

INSERT INTO `mantis_user_pref_table` (`id`, `user_id`, `project_id`, `default_profile`, `default_project`, `refresh_delay`, `redirect_delay`, `bugnote_order`, `email_on_new`, `email_on_assigned`, `email_on_feedback`, `email_on_resolved`, `email_on_closed`, `email_on_reopened`, `email_on_bugnote`, `email_on_status`, `email_on_priority`, `email_on_priority_min_severity`, `email_on_status_min_severity`, `email_on_bugnote_min_severity`, `email_on_reopened_min_severity`, `email_on_closed_min_severity`, `email_on_resolved_min_severity`, `email_on_feedback_min_severity`, `email_on_assigned_min_severity`, `email_on_new_min_severity`, `email_bugnote_limit`, `language`, `timezone`) VALUES
(1, 1, 0, 0, 1, 30, 2, 'ASC', 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'english', 'Europe/Berlin');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_user_print_pref_table`
--

CREATE TABLE `mantis_user_print_pref_table` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `print_pref` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_user_profile_table`
--

CREATE TABLE `mantis_user_profile_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `platform` varchar(32) NOT NULL DEFAULT '',
  `os` varchar(32) NOT NULL DEFAULT '',
  `os_build` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_user_profile_table`
--

INSERT INTO `mantis_user_profile_table` (`id`, `user_id`, `platform`, `os`, `os_build`, `description`) VALUES
(1, 0, 'Platform 2.5', 'Linux', 'Red Hat', 'Linux Red Hat'),
(2, 0, 'Platform 5', 'Windows', 'Windows 7', 'Platform 5 on Windows 7'),
(3, 0, 'Platform 5', 'Windows', 'Windows 10', 'Platform 5 W 10'),
(4, 0, 'Platform ?', 'Windows', 'NT', 'no descr');

-- --------------------------------------------------------

--
-- Struttura della tabella `mantis_user_table`
--

CREATE TABLE `mantis_user_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) NOT NULL DEFAULT '',
  `realname` varchar(191) NOT NULL DEFAULT '',
  `email` varchar(191) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL DEFAULT 1,
  `protected` tinyint(4) NOT NULL DEFAULT 0,
  `access_level` smallint(6) NOT NULL DEFAULT 10,
  `login_count` int(11) NOT NULL DEFAULT 0,
  `lost_password_request_count` smallint(6) NOT NULL DEFAULT 0,
  `failed_login_count` smallint(6) NOT NULL DEFAULT 0,
  `cookie_string` varchar(64) NOT NULL DEFAULT '',
  `last_visit` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `date_created` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `mantis_user_table`
--

INSERT INTO `mantis_user_table` (`id`, `username`, `realname`, `email`, `password`, `enabled`, `protected`, `access_level`, `login_count`, `lost_password_request_count`, `failed_login_count`, `cookie_string`, `last_visit`, `date_created`) VALUES
(1, 'administrator', '', 'root@localhost', 'e5d9dee0892c9f474a174d3bfffb7810', 1, 0, 90, 11, 0, 0, 'zF9uZBxNUsIUsKmdxe8ji_qnTvnDc95YkpN6xiG-fEBcAlLDPd-XdpwP64K4hXmE', 1584008506, 1584002837),
(2, 'vipart', '', '74b37f106646a5ddb501ba5128c1028a@localhost', 'fbc18ec690e3241ed360f24c53a850a7', 1, 0, 25, 0, 0, 0, 'V5pA1CKP5sX4FZh7z-JutyiTxManKOa3-BCaGfIB_9SjvfkPj5GjipCxWWr3qAQX', 1584003480, 1584003480),
(3, 'richbrit', '', '4d9a582c8d228f5ac45a6476b8d5d8de@localhost', '6a6e2bbaba2bd64567df650244288fd4', 1, 0, 25, 0, 0, 0, 'BEV2LtGJSJxcC3wsNcoX_sDiTaSK0k4wmOdHaKx-YZogpN_QsU6bqdF4wi_ay7JU', 1584003480, 1584003480),
(4, 'khojda', '', '5b426f6f57aeed1d9c1906c18139b59c@localhost', '8f6ce351435426b3cf2b0d2a40914a99', 1, 0, 25, 0, 0, 0, 'eQoc6fgPS4JJ0W9qe6HV2Z-bUGLMT_FiQbnhP_1g_3sgZstC6DxNm5s5RzKAqPaB', 1584003481, 1584003481),
(5, 'bobheck11', '', '7896af9623fc21ba7d2e8b2e8508ca3b@localhost', '9e0b1100afccc2ecacd5e53d2f5f954a', 1, 0, 25, 0, 0, 0, 'Ftd_aUiMarDw0yhw4Xz3UuASAwU4eHuiAymDtjpQLHPSp6R7ALLftt4IZQEJFt73', 1584003481, 1584003481),
(6, 'atrol', '', 'd1323a99947edca9476f60dc869ea44b@localhost', '24bbbe17717905acb58c4af7d1de1520', 1, 0, 25, 0, 0, 0, '4gHzrgLeOQ8SEngEQU1ggYLBcKVi6QV2iPJ5YtSGHwX76I1xMbR_6r7zl4ErbXH0', 1584003481, 1584003481),
(7, 'sjazz', '', '598af87762c067d7d27711bcf583c685@localhost', 'ce52325f501b04cab32dc31beb50e516', 1, 0, 25, 0, 0, 0, 'fP1rCOPbz_cecpU7GsstjkcdJl8cgUrl5xzProBtkYUSTik6hf-XXuTV6Kfu-y3G', 1584003482, 1584003482),
(8, 'cproensa', '', '0fc438ace24c510accace9bbadb07a40@localhost', 'c96eef56e63c7b4df45f64b2e96564c2', 1, 0, 25, 0, 0, 0, 'vHdh3Aw8453kUF0RdkcpEBvOxNCYVugO9DYbCLEoY0vdJwEYdoGCuVGPGW2zi-I4', 1584003483, 1584003483);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `mantis_api_token_table`
--
ALTER TABLE `mantis_api_token_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_id_name` (`user_id`,`name`);

--
-- Indici per le tabelle `mantis_bugnote_table`
--
ALTER TABLE `mantis_bugnote_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug` (`bug_id`),
  ADD KEY `idx_last_mod` (`last_modified`);

--
-- Indici per le tabelle `mantis_bugnote_text_table`
--
ALTER TABLE `mantis_bugnote_text_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_bug_file_table`
--
ALTER TABLE `mantis_bug_file_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_file_bug_id` (`bug_id`),
  ADD KEY `idx_diskfile` (`diskfile`);

--
-- Indici per le tabelle `mantis_bug_history_table`
--
ALTER TABLE `mantis_bug_history_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_history_bug_id` (`bug_id`),
  ADD KEY `idx_history_user_id` (`user_id`),
  ADD KEY `idx_bug_history_date_modified` (`date_modified`);

--
-- Indici per le tabelle `mantis_bug_monitor_table`
--
ALTER TABLE `mantis_bug_monitor_table`
  ADD PRIMARY KEY (`user_id`,`bug_id`),
  ADD KEY `idx_bug_id` (`bug_id`);

--
-- Indici per le tabelle `mantis_bug_relationship_table`
--
ALTER TABLE `mantis_bug_relationship_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_relationship_source` (`source_bug_id`),
  ADD KEY `idx_relationship_destination` (`destination_bug_id`);

--
-- Indici per le tabelle `mantis_bug_revision_table`
--
ALTER TABLE `mantis_bug_revision_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_rev_type` (`type`),
  ADD KEY `idx_bug_rev_id_time` (`bug_id`,`timestamp`);

--
-- Indici per le tabelle `mantis_bug_table`
--
ALTER TABLE `mantis_bug_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bug_sponsorship_total` (`sponsorship_total`),
  ADD KEY `idx_bug_fixed_in_version` (`fixed_in_version`),
  ADD KEY `idx_bug_status` (`status`),
  ADD KEY `idx_project` (`project_id`);

--
-- Indici per le tabelle `mantis_bug_tag_table`
--
ALTER TABLE `mantis_bug_tag_table`
  ADD PRIMARY KEY (`bug_id`,`tag_id`),
  ADD KEY `idx_bug_tag_tag_id` (`tag_id`);

--
-- Indici per le tabelle `mantis_bug_text_table`
--
ALTER TABLE `mantis_bug_text_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_category_table`
--
ALTER TABLE `mantis_category_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_category_project_name` (`project_id`,`name`);

--
-- Indici per le tabelle `mantis_config_table`
--
ALTER TABLE `mantis_config_table`
  ADD PRIMARY KEY (`config_id`,`project_id`,`user_id`);

--
-- Indici per le tabelle `mantis_custom_field_project_table`
--
ALTER TABLE `mantis_custom_field_project_table`
  ADD PRIMARY KEY (`field_id`,`project_id`);

--
-- Indici per le tabelle `mantis_custom_field_string_table`
--
ALTER TABLE `mantis_custom_field_string_table`
  ADD PRIMARY KEY (`field_id`,`bug_id`),
  ADD KEY `idx_custom_field_bug` (`bug_id`);

--
-- Indici per le tabelle `mantis_custom_field_table`
--
ALTER TABLE `mantis_custom_field_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_custom_field_name` (`name`);

--
-- Indici per le tabelle `mantis_email_table`
--
ALTER TABLE `mantis_email_table`
  ADD PRIMARY KEY (`email_id`);

--
-- Indici per le tabelle `mantis_filters_table`
--
ALTER TABLE `mantis_filters_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_login_table`
--
ALTER TABLE `mantis_login_table`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `mantis_news_table`
--
ALTER TABLE `mantis_news_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_plugin_table`
--
ALTER TABLE `mantis_plugin_table`
  ADD PRIMARY KEY (`basename`);

--
-- Indici per le tabelle `mantis_project_file_table`
--
ALTER TABLE `mantis_project_file_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_project_hierarchy_table`
--
ALTER TABLE `mantis_project_hierarchy_table`
  ADD UNIQUE KEY `idx_project_hierarchy` (`child_id`,`parent_id`),
  ADD KEY `idx_project_hierarchy_child_id` (`child_id`),
  ADD KEY `idx_project_hierarchy_parent_id` (`parent_id`);

--
-- Indici per le tabelle `mantis_project_table`
--
ALTER TABLE `mantis_project_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_project_name` (`name`),
  ADD KEY `idx_project_view` (`view_state`);

--
-- Indici per le tabelle `mantis_project_user_list_table`
--
ALTER TABLE `mantis_project_user_list_table`
  ADD PRIMARY KEY (`project_id`,`user_id`),
  ADD KEY `idx_project_user` (`user_id`);

--
-- Indici per le tabelle `mantis_project_version_table`
--
ALTER TABLE `mantis_project_version_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_project_version` (`project_id`,`version`);

--
-- Indici per le tabelle `mantis_sponsorship_table`
--
ALTER TABLE `mantis_sponsorship_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_sponsorship_bug_id` (`bug_id`),
  ADD KEY `idx_sponsorship_user_id` (`user_id`);

--
-- Indici per le tabelle `mantis_tag_table`
--
ALTER TABLE `mantis_tag_table`
  ADD PRIMARY KEY (`id`,`name`),
  ADD KEY `idx_tag_name` (`name`);

--
-- Indici per le tabelle `mantis_tokens_table`
--
ALTER TABLE `mantis_tokens_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_typeowner` (`type`,`owner`);

--
-- Indici per le tabelle `mantis_user_pref_table`
--
ALTER TABLE `mantis_user_pref_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_user_print_pref_table`
--
ALTER TABLE `mantis_user_print_pref_table`
  ADD PRIMARY KEY (`user_id`);

--
-- Indici per le tabelle `mantis_user_profile_table`
--
ALTER TABLE `mantis_user_profile_table`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `mantis_user_table`
--
ALTER TABLE `mantis_user_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_cookie_string` (`cookie_string`),
  ADD UNIQUE KEY `idx_user_username` (`username`),
  ADD KEY `idx_enable` (`enabled`),
  ADD KEY `idx_access` (`access_level`),
  ADD KEY `idx_email` (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `mantis_api_token_table`
--
ALTER TABLE `mantis_api_token_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `mantis_bugnote_table`
--
ALTER TABLE `mantis_bugnote_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `mantis_bugnote_text_table`
--
ALTER TABLE `mantis_bugnote_text_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_file_table`
--
ALTER TABLE `mantis_bug_file_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_history_table`
--
ALTER TABLE `mantis_bug_history_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_relationship_table`
--
ALTER TABLE `mantis_bug_relationship_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_revision_table`
--
ALTER TABLE `mantis_bug_revision_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_table`
--
ALTER TABLE `mantis_bug_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT per la tabella `mantis_bug_text_table`
--
ALTER TABLE `mantis_bug_text_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT per la tabella `mantis_category_table`
--
ALTER TABLE `mantis_category_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT per la tabella `mantis_custom_field_table`
--
ALTER TABLE `mantis_custom_field_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `mantis_email_table`
--
ALTER TABLE `mantis_email_table`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT per la tabella `mantis_filters_table`
--
ALTER TABLE `mantis_filters_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `mantis_news_table`
--
ALTER TABLE `mantis_news_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `mantis_project_file_table`
--
ALTER TABLE `mantis_project_file_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `mantis_project_table`
--
ALTER TABLE `mantis_project_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `mantis_project_version_table`
--
ALTER TABLE `mantis_project_version_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `mantis_sponsorship_table`
--
ALTER TABLE `mantis_sponsorship_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `mantis_tag_table`
--
ALTER TABLE `mantis_tag_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `mantis_tokens_table`
--
ALTER TABLE `mantis_tokens_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT per la tabella `mantis_user_pref_table`
--
ALTER TABLE `mantis_user_pref_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `mantis_user_profile_table`
--
ALTER TABLE `mantis_user_profile_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `mantis_user_table`
--
ALTER TABLE `mantis_user_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
