## The Subjects folder contains


**PHP-based Applications:**
- Mantis Bug Tracker App
    - Version: 2.21.0
    - MySQL Database: the database needed to reset the application to initial state 
    - SQL Server Database: same as above
- Dolibarr App 
    - Version: 10.0.0
    - MySQL Database: same as above 
    - SQL Server Database: same as above
- MRBS App
    - Version: 1.9.4
    - MySQL Database: same as above 
    - SQL Server Database: same as above

**How to install/configure PHP Apps:**
1. Place the content of the installation files zip of the chosen application under _htdocs_ folder of your XAMPP (or analogous web server) installation path, then follow the application instructions to install it;  
2. Create/Import into MySQL environment the MySQL database of the chosen application in order to reset its initial state; 
2. Import ('attach' command) into Microsoft SQL Server Management Studio the SQL Server database of the chosen application and give admin permissions to the SQL Server user used by ABT (see [Tools](https://gitlab.com/Sibilla/sibilla/-/tree/master/Tools)) to access the database during the runs.

**Notes**
For our experiments, the local installation of the apps and MySQL settings relied on XAMPP v. 7.3.11 and PhPMyAdmin v. 4.9.1, while SQL Server working copies of the databases were configured using Microsoft SQL Server Management Studio v. 17.9.1

------------------------------------------

**Single-Page Applications:** the apps used for our experiments can be downloaded from their related Github repository
- DimeShift Single-Page App
    - Version: 0.1.42
    - URL: https://github.com/jeka-kiselyov/dimeshift
- Pagekit Single-Page App
    - Version: 1.0.18
    - URL: https://github.com/pagekit/pagekit
- PetClinic Single-Page App
    - Version: 11.2.11
    - URL: https://github.com/spring-petclinic/spring-petclinic-angular
- Retrospected Single-Page App
    - Version: 5.1.2
    - URL: https://github.com/antoinejaussoin/retro-board
- SplittyPie Single-Page App
    - Version: 1.0.0
    - URL: https://github.com/tsubik/splittypie
- Tricentis Vehicle Insurance Demo Single-Page App
    - Version: -
    - URL: http://sampleapp.tricentis.com/101/


**How to install Single-Page Apps:**
1. Just follow the instructions found in the README file within the Github repositories
2. Tricentis Vehicle Insurance Demo does not require any installation








