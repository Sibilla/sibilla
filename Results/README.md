## The Results folder contains


- **RESULTS1** and **RESULTS2** xlsx 
    - they summarize the data from **RESULTS.zip**
    - **RESULTS1**: shows results  (i.e., execution time, deviations, forms submmissions, coverage, near-duplicate cost, % actions saved by Sibilla knowledge base) for _Dolibarr_, _Mantis Bug Tracker_, _MRBS_, and _Tricentis_ applications
    - **RESULTS2**: shows results  (i.e., execution time, deviations, forms submmissions, coverage, near-duplicate cost, % actions saved by Sibilla knowledge base) for _DimeShift_, _Pagekit_, _PetClinic_, _Retrospected_, and _SplittyPie_ applications
- **Plots_Data** folder
    - It containts R scripts and CSV files to reproduce the boxplots in the paper (paths to CSV files in R scripts have to be changed to adapt to the local machine)
    - **NearDuplicate-Cost.R**: it refers to _NearDuplicate-Cost.csv_ to generate Figure 3 in the paper
    - **Execution_Time.R**: it refers to _execution-time-APPNAME.csv_ to generate Figure 4 in the paper
    - **Deviations.R**: it refers to _deviationsperc-APPNAME.csv_ files to generate Figure 5 in the paper
    - **Coverage-Perc-Boxplot.R**: it refers to _cov-dolibarr.csv_, _cov-mantis.csv_ and _cov-mrbs.csv_ to generate Figure 6 in the paper
    - **Coverage-Cost.R**: it is used to generate Figure 7 in the paper
- **savedActions** folder
    - It contains the zipped folders (to unzip) of about _1500_ actions each that were saved by executing the Baseline configuration on each subject application, technique and iteration (10 iteration runs x 9 subject applications x 3 techniques x Baseline configuration). These actions were saved as ground truth and were then replicated by Sibilla configurations.
