library(ggplot2)  
data <- data.frame(values = c(4.79, 2.36, 5.16, 1.47, 28.76, 8.75),  
                   group = rep(c("Actions-5", "Widgets-5", "Baseline"), each = 2), Type = c("w/ Coverage", "w/o Coverage"))
data_base <- reshape(data,idvar = "Type", timevar = "group", direction = "wide")
row.names(data_base) <- data_base$Type
data_base <- data_base[ , 2:ncol(data_base)]
colnames(data_base) <- c("Baseline", "Actions-5", "Widgets-5")
data_base <- as.matrix(data_base)
ggplot(data,
       aes(x = group,
           y = values,
           fill = Type)) +
  geom_bar(stat = "identity",
           position = "dodge",colour="black") + xlab("Configurations") + ylab("Execution Time (hh)")
		   
		   
		   
		   