library(ggplot2)

mantis <- read.csv("cov-mantis.csv", sep=",", row.names=NULL, header=T, check.names=FALSE) 
mrbs <- read.csv("cov-mrbs.csv", sep=",", row.names=NULL, header=T, check.names=FALSE) 
dolibarr <- read.csv("cov-dolibarr.csv", sep=",", row.names=NULL, header=T, check.names=FALSE) 

mantis$Application <- "Mantis"
dolibarr$Application <- "Dolibarr"
mrbs$Application <- "MRBS"

df <- rbind(mantis, dolibarr, mrbs)
df$Configuration <- factor(df$Configurations , levels=c("Actions-5", "Widgets-5", "Baseline"))

plot <- ggplot(df, aes(x=Configurations, y=PercCoverage, fill=Configurations)) +
  ylab("% Coverage") +
  geom_boxplot() + 
  facet_grid(Techniques~Application) + 
  theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1), legend.position="none")
plot